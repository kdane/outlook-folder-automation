
## Install Macros

* Download files from this repo
* activate developer ribbon (right 
	* right click in ribbon -> customize ribbon
	* check the box to activate the "Developer" tab
* Go to development tab on ribbon
	* Click on "Macros" 
	* File -> Import
	* import both "DebugFolderInformation.bas" AND "FolderMove.bas"

## Install Custom Ribbon

* Download files from this repo
* File -> Options -> Customize Ribbon
* On lower right, select "Import/Export" -> import the "Outlook Customizations (olkexplorer).exportedUI"
	* NOTE THAT THIS MAY BLOW UP OTHER RIBBON CUSTOMIZATIONS YOU HAVE PREVIOUSLY COMPLETED (DOING A BACKUP EXPORT IS RECOMMENDED)

## Customize for your needs

* To change the ribbon name
	* right click on Ribbon
	* select "customize ribbon"
	* Right click on the "KRD Automation" tab and select rename
	
* To add new items
	* Get the path 
		* Select the folder you want to send to
		* Click on Developer Tab
		* Macros dropdown -> Select Get Info Macro
		* A Draft Email will open with information on the folder
			* you need the "FolderPath"
			
	* Add the path to the FolderMove Macro
	
	* Add a button to the ribbon
		* right click on Ribbon
		* select "customize ribbon"
		* Choose Command From "Macros"
		* Select Command and CLick "Add"
		* Set the icon graphic
		
		







