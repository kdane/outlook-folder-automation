Attribute VB_Name = "FolderMove"

'****************INDIVIDUAL FOLDER FUNCTIONS**************************

Sub MoveTo_Personal()
    Call NewMoveToFolder("\\KDane@ThorntonTomasetti.com\Inbox\Misc\test")
End Sub

Sub MoveTo_InfoTech()
    Call NewMoveToFolder("\\KDane@ThorntonTomasetti.com\Inbox\01_Digital Design Management\Info Technology")
End Sub


Sub MoveTo_D2Mgmt()
    Call NewMoveToFolder("\\KDane@ThorntonTomasetti.com\Inbox\01_Digital Design Management")
End Sub

Sub MoveTo_Opportunities()
    Call NewMoveToFolder("\\KDane@ThorntonTomasetti.com\Inbox\External Relationships\Opportunities")
End Sub

Sub MoveTo_TicketsSnow()
    Call NewMoveToFolder("\\KDane@ThorntonTomasetti.com\Inbox\01_Digital Design Management\Tickets\Snow")
End Sub

Sub MoveTo_Seattle()
    Call NewMoveToFolder("\\KDane@ThorntonTomasetti.com\Inbox\Seattle Office Info")
End Sub

Sub MoveTo_Proj_CitM()
    Call NewMoveToFolder("\\KDane@ThorntonTomasetti.com\Inbox\Project Work\Project - Citizen M")
End Sub

Sub MoveTo_B555()
    Call NewMoveToFolder("\\KDane@ThorntonTomasetti.com\Inbox\External Relationships\B555 ISOTC 442/WG3\B555 Unread")
End Sub

Sub MoveTo_COREOversight()
    Call NewMoveToFolder("\\KDane@ThorntonTomasetti.com\Inbox\03_Other TT Orgs\CORE\CORE Oversight")
End Sub



'****************GET SELECTED FOLDERS AND MOVE TO RELEVANT FOLDER************************


'Outlook VB Macro to move selected mail item(s) to a target folder
Sub NewMoveToFolder(targetFolderPath)
On Error Resume Next

    Dim ns As Outlook.NameSpace
    Dim MoveToFolder As Outlook.MAPIFolder
    Dim objItem As Outlook.MailItem
    Set ns = Application.GetNamespace("MAPI")

        'get the target folder
          
            'Dim MoveToFolder As Outlook.folder
            'Set folder = GetFolder("\\Mailbox - Dan Wilson\Inbox\Customers")
             Set MoveToFolder = GetFolder(targetFolderPath)
        
        'ensure items are selected
        If Application.ActiveExplorer.Selection.Count = 0 Then
            MsgBox ("No item selected")
            Exit Sub
        End If
        
        'if folder not found
        If MoveToFolder Is Nothing Then
            MsgBox "Target folder not found!", vbOKOnly + vbExclamation, "Move Macro Error"
        End If
        
        'move selected items
        For Each objItem In Application.ActiveExplorer.Selection
            If MoveToFolder.DefaultItemType = olMailItem Then
                If objItem.Class = olMail Then
                    objItem.Move MoveToFolder
                End If
            End If
        Next

    'reset variables
    Set objItem = Nothing
    Set MoveToFolder = Nothing
    Set ns = Nothing

End Sub



'****************GET FOLDER FROM PATH****************
'obtained from https://docs.microsoft.com/en-us/office/vba/outlook/how-to/items-folders-and-stores/obtain-a-folder-object-from-a-folder-path

Function GetFolder(ByVal FolderPath As String) As Outlook.folder
    Dim TestFolder As Outlook.folder
    Dim FoldersArray As Variant
    Dim i As Integer
 
    On Error GoTo GetFolder_Error
    If Left(FolderPath, 2) = "\\" Then
        FolderPath = Right(FolderPath, Len(FolderPath) - 2)
    End If
    
    'Convert folderpath to array
    FoldersArray = Split(FolderPath, "\")
    Set TestFolder = Application.Session.Folders.Item(FoldersArray(0))
    If Not TestFolder Is Nothing Then
        For i = 1 To UBound(FoldersArray, 1)
            Dim SubFolders As Outlook.Folders
            Set SubFolders = TestFolder.Folders
            Set TestFolder = SubFolders.Item(FoldersArray(i))
            If TestFolder Is Nothing Then
                Set GetFolder = Nothing
            End If
        Next
    End If
     
   'Return the TestFolder
    Set GetFolder = TestFolder
    Exit Function
 
GetFolder_Error:
    Set GetFolder = Nothing
    Exit Function
End Function
 
 
Sub Debug_TestGetFolder()
    Dim folder As Outlook.folder
    'Set folder = GetFolder("\\Mailbox - Dan Wilson\Inbox\Customers")
     Set folder = GetFolder("\\KDane@ThorntonTomasetti.com\Inbox\01_Digital Design Management")
    If Not (folder Is Nothing) Then
        folder.Display
    End If
End Sub







